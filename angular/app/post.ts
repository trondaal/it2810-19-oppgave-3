/*
 * Interface for the Post-object. 
 * Sets the type for each of the required properties. 
 */
export interface Post {
	heading: string;
	content: string;
}